/**
 * Created by tariq_tonmoy on 5/17/17.
 */
// jshint ignore: start

function homeCtrl ($window) {
    var chkPoint = 0;
    var vm = this;

    vm.showOverview = false;
    vm.heding = 'Gazi First, Gazi Best';
    vm.msg = 'We strive to be the recognized leader in all the sectors that the Group operates in and clients’ quality expectations in all aspects';
    vm.aboutHeader = 'About Gazi Group';
    vm.overview = {};

    var win=angular.element($window)
    vm.load = function () {
        $('body').css('height',win.height()*1.1);
    }

    vm.loadNextContent = function () {
        chkPoint++;
        if (chkPoint >=0) {
            $('body').css('height','auto');
            vm.showOverview = true;
            vm.overview={
                headers: ['Mission','Vision','Future Plan'],
                keys:['mission','vision','future-plan'],
                imgSrc:['https://static.pexels.com/photos/248159/pexels-photo-248159.jpeg',
                    'https://static.pexels.com/photos/235489/pexels-photo-235489.jpeg',
                    'https://static.pexels.com/photos/217380/pexels-photo-217380.jpeg'],
                videos:['1KzlZtwyXFU','qQrgto184Tk','RBumgq5yVrA'],
                msgs:['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet lobortis justo, ut lobortis risus. Etiam bibendum nunc diam, vel posuere mi scelerisque at. Vestibulum lectus ligula, lobortis ut erat nec, ullamcorper sodales ligula. Suspendisse non massa a tortor lobortis placerat. Phasellus lacinia mi eu volutpat porta. In dictum, sapien id pharetra sodales, dolor tortor euismod ex, eu rhoncus lorem ex at velit. Suspendisse et quam ex. Nulla lobortis dapibus est condimentum egestas. Proin ultrices aliquet dolor at rutrum. Mauris vel erat in quam elementum tincidunt. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed a posuere ligula, mollis tempor augue. Sed tincidunt tincidunt mattis.','Sed sed commodo lectus. Proin sit amet lobortis ligula. Pellentesque lacinia leo eu risus vestibulum semper. In commodo lectus ligula, in accumsan ex mollis a. Pellentesque in orci tincidunt, vestibulum mi vitae, pulvinar mi. Sed mollis dapibus sapien eu sodales. Maecenas fermentum suscipit nisl pretium pretium. Praesent sed elit sit amet ligula ornare tincidunt quis eu sapien. Vestibulum sed orci gravida, tempus sapien vulputate, mollis lorem.','Sed consequat maximus lorem ut consequat. Praesent consequat faucibus risus, sit amet semper lectus pulvinar sit amet. Proin finibus nulla leo, vitae malesuada elit pellentesque vel. Cras purus metus, suscipit vitae erat vel, gravida elementum felis. Quisque non urna et nulla elementum vestibulum. Sed porta metus id gravida ultricies. Phasellus commodo purus nec risus varius, ullamcorper porttitor massa pellentesque. Curabitur fermentum fringilla felis sit amet maximus. Vestibulum sodales fringilla quam eget mattis. Aenean tortor ex, tincidunt sed viverra sit amet, consectetur id lacus. Proin posuere volutpat ex sit amet lacinia. Suspendisse vel vulputate neque, eget ultricies diam. Aenean velit massa, condimentum id lacus ut, eleifend porta lacus. Duis nec libero vehicula tellus condimentum commodo ut in quam. Morbi iaculis nulla vel est dictum, vel luctus neque vestibulum.']
            };
        }
    };
}

function homeConfig ($stateProvider) {
    var home = {
        url: '/',
        views: {
            'header': {
                templateUrl: 'gazi_group_nav/gazi_group_nav.html',
                controller: 'navCtrl'
            },
            'content': {
                templateUrl: 'gazi_group_home/gazi_group_home.html'
            },
            'footer': {
                templateUrl: 'gazi_group_footer/gazi_group_footer.html'
            }
        }
    };
    $stateProvider.state('home', home)
}
angular.module('gaziGroupApp.home', ['ui.router', 'gaziGroupApp.nav'])
    .config(['$stateProvider', homeConfig])
    .controller('homeCtrl', ['$window',homeCtrl]);

