/**
 * Created by tariq_tonmoy on 5/17/17.
 */
// jshint ignore: start

function sister_concernsCtrl () {
    var vm = this
    vm.msg = 'I\'m coming from sister_concerns!!!!!'
}

function sister_concernsConfig ($stateProvider) {
    var sister_concerns = {
        url: '/sister_concerns',
        views: {
            'header': {
                templateUrl: 'gazi_group_nav/gazi_group_nav.html',
                controller: 'navCtrl'
            },
            'content': {
                templateUrl: 'gazi_group_sister_concerns/gazi_group_sister_concerns.html'
            },
            'footer': {
                templateUrl: 'gazi_group_footer/gazi_group_footer.html'
            }
        }
    }
    $stateProvider.state('sister_concerns', sister_concerns)
}
angular.module('gaziGroupApp.sister_concerns', ['ui.router', 'gaziGroupApp.nav'])
    .config(['$stateProvider', sister_concernsConfig])
    .controller('sister_concernsCtrl', sister_concernsCtrl)
