// jshint ignore: start
'use strict';
function gazi_group_nav_config ($urlRouterProvider) {
    $urlRouterProvider.otherwise('/')
}

angular.module('gaziGroupApp', [
    'ui.router',
    'ngAnimate',
    'angular-loading-bar',
    'infinite-scroll',
    'gaziGroupApp.home',
    'gaziGroupApp.contact',
    'gaziGroupApp.directors',
    'gaziGroupApp.group_contacts',
    'gaziGroupApp.management',
    'gaziGroupApp.other_concerns',
    'gaziGroupApp.sister_concerns',
    'gaziGroupApp.welfare',
    'gaziGroupApp.directives'])
    .config(['$urlRouterProvider', gazi_group_nav_config]);



