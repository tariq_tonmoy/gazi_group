/**
 * Created by tariq_tonmoy on 5/17/17.
 */
// jshint ignore: start

function welfareCtrl () {
    var vm = this
    vm.msg = 'I\'m coming from welfare!!!!!'
}
function welfareConfig ($stateProvider) {
    var welfare = {
        url: '/welfare',
        views: {
            'header': {
                templateUrl: 'gazi_group_nav/gazi_group_nav.html',
                controller: 'navCtrl'
            },
            'content': {
                templateUrl: 'gazi_group_welfare/gazi_group_welfare.html'
            },
            'footer': {
                templateUrl: 'gazi_group_footer/gazi_group_footer.html'
            }
        }
    }
    $stateProvider.state('welfare', welfare)
}
angular.module('gaziGroupApp.welfare', ['ui.router','gaziGroupApp.nav'])
    .config(['$stateProvider', welfareConfig])
    .controller('welfareCtrl',welfareCtrl)
