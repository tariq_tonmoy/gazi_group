/**
 * Created by tariq_tonmoy on 5/17/17.
 */
// jshint ignore: start

function group_contactsCtrl () {
    var vm = this
    vm.msg = 'I\'m coming from group_contacts!!!!!'
}

function group_contactsConfig ($stateProvider) {
    var group_contacts = {
        url: '/group_contacts',
        views: {
            'header': {
                templateUrl: 'gazi_group_nav/gazi_group_nav.html',
                controller: 'navCtrl'
            },
            'content': {
                templateUrl: 'gazi_group_group_contacts/gazi_group_group_contacts.html'
            },
            'footer': {
                templateUrl: 'gazi_group_footer/gazi_group_footer.html'
            }
        }
    }
    $stateProvider.state('group_contacts', group_contacts)
}
angular.module('gaziGroupApp.group_contacts', ['ui.router', 'gaziGroupApp.nav'])
    .config(['$stateProvider', group_contactsConfig])
    .controller('group_contactsCtrl', group_contactsCtrl)
