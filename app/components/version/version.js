'use strict';

angular.module('gaziGroupApp.version', [
  'gaziGroupApp.version.interpolate-filter',
  'gaziGroupApp.version.version-directive'
])

.value('version', '0.1');
