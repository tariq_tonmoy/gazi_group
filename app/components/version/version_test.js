'use strict';

describe('gaziGroupApp.version module', function() {
  beforeEach(module('gaziGroupApp.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
