/**
 * Created by tariq_tonmoy on 5/17/17.
 */
// jshint ignore: start

function contactCtrl () {
    var vm = this
    vm.msg = 'I\'m coming from contact!!!!!'
}


function contactConfig ($stateProvider) {
    var contact = {
        url: '/contact',
        views: {
            'header': {
                templateUrl: 'gazi_group_nav/gazi_group_nav.html',
                controller: 'navCtrl'
            },
            'content': {
                templateUrl: 'gazi_group_contact/gazi_group_contact.html'
            },
            'footer': {
                templateUrl: 'gazi_group_footer/gazi_group_footer.html'
            }
        }
    }
    $stateProvider.state('contact', contact)
}
angular.module('gaziGroupApp.contact', ['ui.router','gaziGroupApp.nav'])
    .config(['$stateProvider', contactConfig])
    .controller('contactCtrl', contactCtrl);
