/**
 * Created by tariq_tonmoy on 5/17/17.
 */
// jshint ignore: start
function navCtrl () {
    var vm = this;
    vm.msg='Hello from NAV!';
    vm.home='Home';
    vm.sister_concerns="Sister Concerns";
    vm.other_concerns="Other Concerns";
    vm.group_contacts="Group Contacts";
    vm.management="Management";
    vm.directors="Directors";
    vm.welfare="Welfare";
    vm.contact="Contact Us";
    vm.concerns="Concerns";
    vm.people="People";

}
angular.module('gaziGroupApp.nav',[])
    .controller('navCtrl', [navCtrl]);
