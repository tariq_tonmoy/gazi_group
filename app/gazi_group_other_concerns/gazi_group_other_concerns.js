/**
 * Created by tariq_tonmoy on 5/17/17.
 */
// jshint ignore: start

function other_concernsCtrl () {
    var vm = this
    vm.msg = 'I\'m coming from other_concerns!!!!!'
}

function other_concernsConfig ($stateProvider) {
    var other_concerns = {
        url: '/other_concerns',
        views: {
            'header': {
                templateUrl: 'gazi_group_nav/gazi_group_nav.html',
                controller: 'navCtrl'
            },
            'content': {
                templateUrl: 'gazi_group_other_concerns/gazi_group_other_concerns.html'
            },
            'footer': {
                templateUrl: 'gazi_group_footer/gazi_group_footer.html'
            }
        }
    }
    $stateProvider.state('other_concerns', other_concerns)
}
angular.module('gaziGroupApp.other_concerns', ['ui.router', 'gaziGroupApp.nav'])
    .config(['$stateProvider', other_concernsConfig])
    .controller('other_concernsCtrl', other_concernsCtrl)
