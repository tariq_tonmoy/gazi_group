/**
 * Created by tariq_tonmoy on 5/17/17.
 */
// jshint ignore: start

function directorsCtrl () {
    var vm = this
    vm.msg = 'I\'m coming from directors!!!!!'
}

function directorsConfig ($stateProvider) {
    var directors = {
        url: '/directors',
        views: {
            'header': {
                templateUrl: 'gazi_group_nav/gazi_group_nav.html',
                controller: 'navCtrl'
            },
            'content': {
                templateUrl: 'gazi_group_directors/gazi_group_directors.html'
            },
            'footer': {
                templateUrl: 'gazi_group_footer/gazi_group_footer.html'
            }
        }
    }
    $stateProvider.state('directors', directors)
}
angular.module('gaziGroupApp.directors', ['ui.router', 'gaziGroupApp.nav'])
    .config(['$stateProvider', directorsConfig])
    .controller('directorsCtrl', directorsCtrl)
