/**
 * Created by tariq_tonmoy on 5/17/17.
 */
// jshint ignore: start

function managementCtrl () {
    var vm = this
    vm.msg = 'I\'m coming from management!!!!!'
}

function managementConfig ($stateProvider) {
    var management = {
        url: '/management',
        views: {
            'header': {
                templateUrl: 'gazi_group_nav/gazi_group_nav.html',
                controller: 'navCtrl'
            },
            'content': {
                templateUrl: 'gazi_group_management/gazi_group_management.html'
            },
            'footer': {
                templateUrl: 'gazi_group_footer/gazi_group_footer.html'
            }
        }
    }
    $stateProvider.state('management', management)
}
angular.module('gaziGroupApp.management', ['ui.router', 'gaziGroupApp.nav'])
    .config(['$stateProvider', managementConfig])
    .controller('managementCtrl', managementCtrl)
