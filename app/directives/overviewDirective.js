/**
 * Created by tariq on 5/23/17.
 */
//jshint ignore:start

const states = {
    MOUSE_OUT: 0,
    MOUSE_IN: 1,
    MOUSE_IN_ANIM_PLAYED: 2,
    MOUSE_OUT_ANIM_PLAYED: 3,
    MOUSE_IN_VID_PLAYED: 4
}
const colors = {
    OVERVIEW_BG: 'rgba(0, 85, 167, .3)',
    OVERVIEW_TXT_HIGHLIGHT: 'rgba(0,85,167,1)',
    OVERVIEW_TXT: 'rgba(230,230, 230, .3)',
    OVERVIEW_BG_HIGHLIGHT: 'rgba(230,230, 230, .8)',
    TEST: 'rgba(255,0,0,1)'

}
const keys = ['mission', 'vision', 'future-plan']
const ids = ['#mission', '#vision', '#future-plan']
const col = 3


function getImgContainer(elem, type) {
    if ($(elem).is(type)) {
        return elem
    }
    else return $(elem).find(type)
}


function overviewDirective($window) {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            overview: '='
        },
        templateUrl: 'templates/overviewTemplate.html',
        link: function (scope, elem, attr) {
            var winW
            var win = angular.element($window)
            var ifrmDiv = $(elem).find('.screen')

            var tag = document.createElement('script')

            tag.src = 'https://www.youtube.com/iframe_api'
            var firstScriptTag = document.getElementsByTagName('script')[0]
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)


            scope.player

            function onPlayerReady(event) {
                event.target.playVideo()
                event.target.setVolume(0)
            }


            function onPlayerStateChange(event) {
                if (event.data == YT.PlayerState.ENDED) {
                    event.target.playVideo()
                }
            }

             window.onYouTubeIframeAPIReady=function() {
                scope.player = new YT.Player('vid-player', {
                    playerVars: { 'modestbranding': 1, 'controls': 0, 'showinfo':0 },
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    }
                })
            }

             scope.loadVid = function(vidId){
                scope.player.loadVideoById(vidId, 0, "large")
            }
             scope.playerSize = function(width, height){
                scope.player.setSize(width, height)
            }
            scope.pause = function(){
                scope.player.pauseVideo()
            }



            function resetIfrm() {
                ifrmDiv.css('width', winW)
                ifrmDiv.css('height', winW / col)

                scope.vid_player_wid = winW
                scope.vid_player_height = (winW * 9 / 16)
                scope.vid_player_margin_top = (winW * (1 / col - 9 / 16)) / 2
                scope.vid_player_margin_bottom = (winW * (9 / 16 - 1 / col)) / 2
            }


            elem.ready(function () {
                winW = win.width()
                $('#vid-player').addClass('remove-from-screen')
                resetIfrm()
            })

            angular.element($window).on('resize', function () {
                winW = win.width()
                resetIfrm()
            })
        }
    }
}

function hvrDirective($window) {
    var activeElem;

    return {
        link: function (scope, elem, attr) {
            var window = angular.element($window)
            var state = states.MOUSE_OUT
            var timer
            var delay = 100
            var vidTimer
            var vidDelay = 5000
            var imageContainers = []

            function setVideoPlayer() {
                if (window.width() <= 991)return
                $('#vid-player').removeClass('remove-from-screen')

                scope.playerSize(scope.vid_player_wid,scope.vid_player_height)
                $('#vid-player').css('margin-top',scope.vid_player_margin_top)
                $('#vid-player').css('margin-bottom',scope.vid_player_margin_bottom)

                scope.loadVid(attr.vidId)
            }

            function fixAspectRatio() {
                if (window.width() >= 991) {
                    $(elem).css('height', window.width() / col)
                    $(elem).find('.overflow-msg-container').css('height', window.width() * 70 / (100 * col))
                }
            }

            function getImageContainerList() {
                for (var i = 0; i < col; i++) {
                    var e = document.getElementById(keys[i]);
                    imageContainers.push(getImgContainer(e, 'img'))
                }
            }

            window.on('resize', function () {
                fixAspectRatio()
            })

            elem.ready(function () {
                fixAspectRatio()
                $(elem).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                    function () {
                        $(elem).removeClass('fadeInUp')
                        $(elem).css('animation-delay', '0ms')
                        $(elem).addClass('initFill')
                        $(elem).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                            function () {
                                $(elem).removeClass('initFill')
                                $(elem).css('background', colors.OVERVIEW_BG)
                            })
                    })
            })


            function playVidEnterAnim(elem) {
                if (window.width() <= 991)return

                setVideoPlayer()

                $(elem).css('color', colors.OVERVIEW_TXT_HIGHLIGHT)


                state = states.MOUSE_IN_VID_PLAYED
                if (imageContainers.length !== col)
                    getImageContainerList()
                for (var i = 0; i < col; i++) {
                    var e = imageContainers[i]
                    $(e).addClass('animated flipOutY')
                }
            }

            function playVidLeaveAnim(elem) {
                $(elem).removeClass('overview-bg-hl')
                $(elem).find('.overview-underline-from-left').css('width', '0%')

                $(elem).css('color', colors.OVERVIEW_TXT)
                $(elem).css('background', colors.OVERVIEW_BG)
                if (imageContainers.length !== col)
                    getImageContainerList()
                for (var i = 0; i < col; i++) {
                    var e = imageContainers[i]
                    $(e).removeClass('animated flipOutY')
                    $(e).addClass('zoomOutImage')
                    $(e).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                        function (event) {
                            $(event.target).removeClass('zoomOutImage')

                        })

                }

            }

            function playEnterAnim(elem) {
                state = states.MOUSE_IN_ANIM_PLAYED
                $(elem).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                    function () {
                        $(elem).removeClass('emph')
                        if (activeElem !== elem)return
                        $(elem).css('color', colors.OVERVIEW_TXT_HIGHLIGHT)
                        $(elem).addClass('overview-bg-hl')
                        $(elem).find('.overview-underline-from-left').css('width', '100%')
                    })
                $(elem).addClass('emph')
                $(elem).find('.overview-underline-from-left').removeClass('width-00')
                $(elem).find('.overview-underline-from-left').addClass('overview-underline-from-left-anim')

                vidTimer = setTimeout(function () {
                    playVidEnterAnim(elem)
                }, vidDelay)
            }

            function playLeaveAnim(elem) {
                $(elem).removeClass('overview-bg-hl')
                $(elem).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                    function () {
                        $(elem).css('color', colors.OVERVIEW_TXT)
                        $(elem).css('background', colors.OVERVIEW_BG)
                        $(elem).removeClass('reverse-emph')
                    })
                $(elem).addClass('reverse-emph')
            }


            elem.on('mouseenter', function () {
                activeElem = elem;
                state = states.MOUSE_IN
                timer = setTimeout(function () {
                    playEnterAnim(elem)
                }, delay)

            })
            elem.on('mouseleave', function () {
                activeElem = null
                $('#vid-player').addClass('remove-from-screen')
                $(elem).find('.overview-underline-from-left').addClass('width-00')
                $(elem).find('.overview-underline-from-left').removeClass('overview-underline-from-left-anim')
                $(elem).removeClass('emph')
                if (state === states.MOUSE_IN_ANIM_PLAYED) {
                    playLeaveAnim(elem)
                    clearTimeout(vidTimer)
                }
                else if (state === states.MOUSE_IN) {
                    clearTimeout(timer)
                    clearTimeout(vidTimer)
                    state = states.MOUSE_OUT
                }
                else if (state === states.MOUSE_IN_VID_PLAYED) {
                    try {
                        scope.pause()
                    } catch (err) {
                        console.error('error in pause')
                    }
                    playVidLeaveAnim(elem)
                }
            })

        }
    }
}


function imageZoomDirective() {

    var state = states.MOUSE_OUT
    var timer
    var delay = 100

    function playEnterAnim(elem) {
        state = states.MOUSE_IN_ANIM_PLAYED
        var img = getImgContainer(elem, 'img')
        img.addClass('zoomInImage')
        $(elem).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function () {
                img.css('height', '125%')
                img.css('width', '125%')
                img.css('top', '-12.5%')
                img.css('bottom', '12.5%')

                img.removeClass('zoomInImage')
            })
    }

    function playLeaveAnim(elem) {
        var img = getImgContainer(elem, 'img')
        img.addClass('zoomOutImage')
        $(elem).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function () {
                img.css('height', '100%')
                img.css('width', '100%')
                img.css('top', '0')
                img.css('bottom', '0')
                img.removeClass('zoomOutImage')
            })
    }

    var link = function (scope, elem, attr) {
        elem.on('mouseenter', function () {
            state = states.MOUSE_IN
            timer = setTimeout(function () {
                playEnterAnim(elem)
            }, delay)
        })
        elem.on('mouseleave', function () {
            if (state === states.MOUSE_IN_ANIM_PLAYED)
                playLeaveAnim(elem)
            else if (state === states.MOUSE_IN) {
                clearTimeout(timer)
                state = states.MOUSE_OUT
            }
        })
    }
    return {
        link: link
    }
}

angular.module('gaziGroupApp.directives', [])
    .directive('overviewDirective', ['$window', overviewDirective])
    .directive('hoverDirective', ['$window', hvrDirective])
    .directive('imageZoomDirective', imageZoomDirective)

