# Featured Pages

## [Home page](http://gazigroupapp.azurewebsites.net/#!/)

This page deploys initial impression for the Gazi Group. Interesting fact about this page is in the three images filling the whole width of the screen.
If users keep mouse over any of the the images for ten seconds, a youtube video starts playing in the background.

## [Product Feature Page](http://gazigroupapp.azurewebsites.net/#!/tanks)
This page showcases products of Gazi tanks. This page has masonry view of the product. Each product has two images in each card. If users hover over any of the images, the backward images shifts left in the background fiving a 3D impression.

