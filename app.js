/**
 * Created by tariq_tonmoy on 5/30/17.
 */
//jshint ignore:start

var express = require('express');
var app = express();
app.use(express.static(__dirname + '/app'));
app.listen(process.env.PORT || 3000);
